# Cool Mint 21 "Vanessa" Cinnamon (Version 1.3 - 04/04/2023)

Cool Mint 21 "Vanessa" Cinnamon Version is a post-install useful scripts collection for Linux Mint which simplify the installation process of various applications.

# How to install

Download button at top right or via git (or just download the zip and uncompress)

    sudo apt install git (for debian based distros)

    create a folder in your HOME one, example:

    $ mkdir $HOME/DATA

    then:

    $ cd $HOME/DATA
	$ git clone https://gitlab.com/thegreatyellow67/cool-mint-21.git

Done!

# 1 Software installation

Use the main script <b>menu</b>: 

    $ ./menu

do not forget to type "./" in front of name script.

# F  A  Q

<b>What can you do if the script does not execute?</b>

Since I sometimes forget to make the script executable, I include here what you can do to solve that.

A script can only run when it is marked as an executable.

	ls -al 

Above code will reveal if a script has an "x". X meaning executable.
Google "chmod" and "execute" and you will find more info.

For now if this happens, you should apply this code in the terminal and add the file name.

	chmod +x typeyourfilename

Then you can execute it by typing

	./typeyourfilename
